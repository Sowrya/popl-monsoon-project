# POPL Project (Group - 3)

## Members

- Aaron     20171207
- Archit    2019121005
- Pratik    20161135
- Sowrya    20161029

## [Try It Online](https://sowrya.gitlab.io/popl-monsoon-project)


## [Demo Video](https://youtu.be/3PhoraKSXME)
[![Alt text](https://img.youtube.com/vi/3PhoraKSXME/0.jpg)](https://www.youtube.com/watch?v=3PhoraKSXME)


## Installation

```bash
npm install
```

## Development

```bash
npm run dev
```

## Production

```bash
npm run prod
```
## Instructions For Interaction with the System

### NFA
    In this experiment, we will allow the user to define a NFA using the GUI and draw the user defined NFA. The NFA is 5 tuple machine <Q,Qi, QF, Σ,  δ> 
    1. The user needs to select the number of states (Q) using the slider. The minimum states can be 1 while the maximum states can be 10.
    2. The user then needs to select the Initial state (Qi) from the drop down menu.
    3. Next the user needs to select the Final States (QF). The user can select more than one state as the final (Accept) state.
    4. Now the next tupple in the NFA Definition is the Alphabets (Σ). The user can enter the alphabets by typing them in the text box and then pressing enter. Note that user can also select ϵ using the drop down in the text box.
    5. Now the final tuple is transition Function(δ). The user can add the transition using the add transition button. Clicking the button a bottom sheet will come up asking the user for the from state, To state and the symbol on which transition should occur. Clicking the Add button will add the transition to the table.
    6. Now as the user keeps adding the transition the NFA will be drawn. To obtain the final NFA user needs to click the DRAW button.
    + Users can also use the download button to download the above user defined NFA, and keep it in his local drive.
    + Later the downloaded file can be uploaded to the page for quickly redefining the Same NFA saving the user a lot of time and energy.
### NFA Simulator
    The NFA simulator works when the user has defined an NFA, otherwise it shows an error. 
    The user can enter the string which he wants to be simulated on the NFA. Then the user needs to press the button next to the text box to initialize the simulator.
    1. The user after clicking the start button will be shown the NFA below with the possible controls.
    2. The Next and previous button will be there to move the user to the next or previous state of machine. The Nodes in the NFA graph will be colored based on the current, Possible Next States and unreachable states. Once the machine finished the simulation it will turn the last state in RED or GREEN based on the Rejection and Acceptance respectively.
    3. There will also be certain values shown in the simulator which will show the current status of the machine.
    4. The user needs to choose the symbol to be consumed and the next desired state so that he can move to the next possible state in simulation.
    5. If the user finds that machine has rejected the input and he wants to change one of the previous decisions, he can directly click the previous button and move to the previous state, and make a different choice this time before clicking next.
    6. Finally user can try the above steps as many times as he wants till the state becomes accepted.
    7. The simulator keeps in mind the machine is Non-determinisitic in nature and proceeds on the choice the user makes.

### NFA To DFA
    1. The user either creates an NFA or uploads a previously downloaded NFA.
    2. If the user clicks on "DFA SIMULATION", he can simulate the formation of DFA by selecting
        - The current state
        - alphabet
        - the reachable states from the current state by reading the alphabet. 
            - The reachable states are to be typed in the format 'state_1<space>state_2....'
    3. The transition table in the DFA SIMULATION gets updated if the reachable states are correctly entered by the user, else an error pop up is shown.

    4. There is a DFA transition table after this section for the NFA, in case the user wants to check the transitions in the DFA.
    5. The DFA is also present in the sectin below the second transition table.
