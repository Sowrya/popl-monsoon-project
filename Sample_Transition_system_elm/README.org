* About the Project
  1. =TreeVis.elm= is a program written using literate programming practices.
  2. To view HTML presentation just open =TreeVis.html=.

* How to run the elm project
  1. Tangle the code from the =TreeVis.org= using the kbd shortcut
     =C-c C-v C-t=.
  2. Now open terminal in the current directory and type =elm reactor=
     and hit enter.
  3. Open =localhost:8000= in the Browser and click on the file name
     =TreeVis.elm=.
